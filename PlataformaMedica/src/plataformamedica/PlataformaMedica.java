/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plataformamedica;

import Usuarios.Asistente;
import Usuarios.Estandar;
import Usuarios.Paciente;
import Usuarios.Preferencial;
import Usuarios.Usuario;
//import static com.sun.javafx.animation.TickCalculation.sub;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author Natanael
 */
public class PlataformaMedica {
//
//    protected static ArrayList<Preferencial> preferenciales = new ArrayList<>();
//    protected static ArrayList<Estandar> estandar = new ArrayList<>();
//    protected static ArrayList<Asistente> asistentes = new ArrayList<>();

    protected static ArrayList<Usuario> usuarios = new ArrayList<Usuario>(4);

    public static void main(String[] args) {
        // TODO code application logic here
        //Archivos.Lectura("usuarios.txt");
        Scanner sc = new Scanner(System.in);

        //LeerFichero ficheroUsuario = new LeerFichero();
        //ficheroUsuario.LeeFicheroUsuario("usuarios.txt", usuarios);
        PlataformaMedica p1 = new PlataformaMedica();

        System.out.println("◘------------------○Bienvenidos○----------------------◘");
        System.out.println("Ingrese su usuario y contraseña para ingresar al sistema.");
        Usuario u = (Usuario) p1.validarUsuario();
        if (u instanceof Paciente) {
            ((Paciente) u).menu((Paciente) u);
        } else {

            ((Asistente) u).menu((Asistente) u, usuarios);

        }
    }

    public Object validarUsuario() {
        Scanner sc = new Scanner(System.in);
        boolean val = false;
        Usuario u1 = null;
        int count = 0;
        while (val == false) {
            System.out.print("♦Usuario:");
            String us = sc.nextLine();
            System.out.print("♦Contraseña:");
            String contra = sc.nextLine();

            LeerFichero ficheroUsuario = new LeerFichero();
            ficheroUsuario.LeeFicheroUsuario("usuarios.txt", usuarios);
            for (Usuario u : usuarios) {
                if (us.equals(u.getUsuario()) && contra.equals(u.getContraseña())) {
                    u1 = u;
                    val = true;
                } 
            }
            if (val == false) {
                System.out.println("Usuario y/o contraseña incorrecta!");
            }

        }
        return u1;

    }

    
}
