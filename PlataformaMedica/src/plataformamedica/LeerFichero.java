/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plataformamedica;

import Usuarios.Asistente;
import Usuarios.Estandar;
import Usuarios.Paciente;
import Usuarios.Preferencial;
import Usuarios.Usuario;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author PC
 */
public class LeerFichero {

    public boolean LeerPerfilPaciente(String nombrearchivo, String usuario) {
        ArrayList<String> usuarios = new ArrayList<>();

        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {

            archivo = new File(nombrearchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            br.readLine();

            while ((linea = br.readLine()) != null) {

                String[] datosUsuario = linea.split(",");

                usuarios.add(datosUsuario[0]);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return usuarios.contains(usuario);

    }

    public ArrayList<String> LeeNombres(String nombrearchivo) {
        ArrayList<String> nombres = new ArrayList<>();

        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {

            archivo = new File(nombrearchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            br.readLine();

            while ((linea = br.readLine()) != null) {

                String[] datosUsuario = linea.split(",");

                nombres.add(datosUsuario[3]);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return nombres;

    }

    public void LeeFicheroUsuario(String nombrearchivo, ArrayList<Usuario> listaUsuario) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {

            archivo = new File(nombrearchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            br.readLine();
            while ((linea = br.readLine()) != null) {

                String[] datosUsuario = linea.split(";");

                if (datosUsuario[4].equals("P")) {
                    listaUsuario.add(new Preferencial(datosUsuario[0], datosUsuario[1], datosUsuario[2], datosUsuario[3],
                            datosUsuario[4]));

                }
                if (datosUsuario[4].equals("E")) {
                    listaUsuario.add(new Estandar(datosUsuario[0], datosUsuario[1], datosUsuario[2], datosUsuario[3],
                            datosUsuario[4]));
                }
                if (datosUsuario[4].equals("A")) {
                    listaUsuario.add(new Asistente(datosUsuario[0], datosUsuario[1], datosUsuario[2], datosUsuario[3],
                            datosUsuario[4]));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

    }

    public String extraerNombre(String nombrearchivo, String codigo) {
        String cod = null;
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {

            archivo = new File(nombrearchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            br.readLine();
            while ((linea = br.readLine()) != null) {

                String[] datosUsuario = linea.split(",");
                if (datosUsuario[0].equals(codigo)) {
                    cod=datosUsuario[3];

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return cod;

    }
    public String extraerNombre1(String nombrearchivo, String usuario) {
        String cod = null;
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {

            archivo = new File(nombrearchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            br.readLine();
            while ((linea = br.readLine()) != null) {

                String[] datosUsuario = linea.split(",");
                if (datosUsuario[3].equals(usuario)) {
                    cod=datosUsuario[3];

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return cod;

    }

    public void leerDoctores(String nombrearchivo, ArrayList<String> listaDoctor) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {

            archivo = new File(nombrearchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            System.out.println("-----------------DOCTORES-------------------");
            System.out.println("------Presione F para volver al menu--------");
            br.readLine();
            int i = 1;
            while ((linea = br.readLine()) != null) {

                System.out.println(i + ")" + linea);
                listaDoctor.add(linea);
                i++;
            }

            System.out.print("Escoja Doctor:");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

    }

    // DESPLIEGA LOS HORARIOS DISPONIBLES EN EL FICHERO DE "HORARIO "
    public void leerHorarios(String nombrearchivo, ArrayList<String> listaHorario) {

        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {

            archivo = new File(nombrearchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            System.out.println("-----------------Horarios-------------------");
            System.out.println("------Presione F para volver al menu--------");
            br.readLine();
            int i = 1;
            while ((linea = br.readLine()) != null) {

                System.out.println(i + ")" + linea);
                i++;
                listaHorario.add(linea);

            }
            System.out.print("Escoja Horario/opcion para volver al menu:");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void ConsultarCita(String nombrearchivo, String codigo) {
        ArrayList<String> lineas = new ArrayList<String>();
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        boolean valor = false;
        try {

            archivo = new File(nombrearchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            br.readLine();
            int i = 0;

            while ((linea = br.readLine()) != null && i == 0) {
                lineas.add(linea);

            }

            for (String x : lineas) {
                String[] datos = x.split(",");
                if (datos[0].toString().equals(codigo)) {
                    System.out.println("-------CITA PENDIENTE-------");
                    System.out.println("Especialidad:" + datos[1] + "\nDoctor:" + datos[2]
                            + "\nFecha:" + datos[4] + "\nHora:" + datos[5]);
                    i++;
                    valor = true;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        if (valor == false) {
            System.out.println("El codigo es erroneo o no existe!");
        }
    }

    public void CancelarCita(String nombrearchivo, String nombreArchivo, String codigo, String usuario) {
        ArrayList<String> lineas = new ArrayList<String>();
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        boolean valor = false;
        try {

            archivo = new File(nombrearchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            br.readLine();
            int i = 0;

            while ((linea = br.readLine()) != null && i == 0) {
                lineas.add(linea);
            }

            for (String x : lineas) {
                String[] datos = x.split(",");
                if (datos[0].toString().equals(codigo)) {
                    FileWriter fichero = null;
                    BufferedWriter bw = null;
                    PrintWriter pw = null;
                    try {
                        fichero = new FileWriter(nombreArchivo, true);
                        bw = new BufferedWriter(fichero);
                        bw.write("\n" + codigo + "," + usuario);

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            // Nuevamente aprovechamos el finally para 
                            // asegurarnos que se cierra el fichero.
                            if (null != fichero) {
                                //fichero.close();
                                bw.close();
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }

                    valor = true;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        if (valor == false) {
            System.out.println("El codigo es erroneo o no existe!");
        } else {
            System.out.println("La solicitud ha sido enviada con exito!");
        }

    }

    public void CrearCita(String nombreArchivo, String codigo, String especialidad,
            String doctor, String paciente, String fechaHora) {
        FileWriter fichero = null;
        BufferedWriter bw = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter(nombreArchivo, true);
            bw = new BufferedWriter(fichero);
            String[] datos = {codigo, especialidad, doctor, paciente, fechaHora};
            String cita = "\n" + datos[0];

            for (int i = 1; i < datos.length; i++) {
                cita += "," + datos[i];
            }
            System.out.println(cita);
            bw.write(cita + "\n");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    //fichero.close();
                    bw.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

    }

    public static void actualizarPaciente(String nombreArchivo, Paciente paciente) {
        FileWriter fichero = null;
        BufferedWriter bw = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter(nombreArchivo, true);
            bw = new BufferedWriter(fichero);
            String datos = paciente.toString();
            String cita = "\n" + datos;

            //System.out.println(cita);
            bw.write(cita + "\n");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    //fichero.close();
                    bw.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

    }

    public ArrayList<String> SacarUsusarioPendientes(String nombrearchivo, String usuario) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        ArrayList<String> usuarios = new ArrayList<>();
        try {

            archivo = new File(nombrearchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            br.readLine();

            while ((linea = br.readLine()) != null) {

                String[] listaDatos = linea.split(";");
                if (!listaDatos[2].equals(usuario)) {
                    usuarios.add(linea);

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }

        }
        return usuarios;

    }

    public void Borrarusuario(String nombrearchivo, String usuario, ArrayList<String> usuarios) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            archivo = new File(nombrearchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            fichero = new FileWriter(nombrearchivo);
            pw = new PrintWriter(fichero);

            String linea;
            br.readLine();

            while ((linea = br.readLine()) != null) {
                pw.write("");
            }
            pw.write("Nombre;Apellido;Usuario;Contrasena;Tipo");
            for (String usuarioEnLista : usuarios) {
                pw.write("\n" + usuarioEnLista);

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }

        }

    }

    public ArrayList<String> extraerCita(String nombrearchivo, String codigo) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        ArrayList<String> codigos = new ArrayList<>();
        try {

            archivo = new File(nombrearchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            br.readLine();

            while ((linea = br.readLine()) != null) {

                String[] listaDatos = linea.split(",");
                if (!listaDatos[0].equals(codigo)) {
                    codigos.add(linea);

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }

        }
        return codigos;

    }

    public void Borrarcita(String nombrearchivo, String codigo, ArrayList<String> codigos) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            archivo = new File(nombrearchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            fichero = new FileWriter(nombrearchivo);
            pw = new PrintWriter(fichero);

            String linea;
            br.readLine();

            while ((linea = br.readLine()) != null) {
                pw.write("");
            }
            pw.write("codigo,especialida,doctor,paciente,fecha,horario");
            for (String cod : codigos) {
                pw.write("\n" + cod);

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }

        }

    }
    public void eliminarSolicitud(String nombrearchivo, String codigo, ArrayList<String> codigos){
 File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            archivo = new File(nombrearchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            fichero = new FileWriter(nombrearchivo);
            pw = new PrintWriter(fichero);

            String linea;
            br.readLine();

            while ((linea = br.readLine()) != null) {
                pw.write("");
            }
            pw.write("codigo,especialida,doctor,paciente,fecha,horario");
            for (String cod : codigos) {
                pw.write("\n" + cod);

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }

        }

    }
    



}
