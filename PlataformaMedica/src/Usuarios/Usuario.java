/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuarios;

//import static com.sun.javafx.animation.TickCalculation.sub;
import java.util.ArrayList;
import java.util.Scanner;


/**
 *
 * @author Natanael
 */
public abstract class Usuario {


   protected String nombre;
   protected String apellido;
   protected String usuario;
   protected String contraseña;
   protected String tipo;

  

    public Usuario(String nombre, String apellido, String usuario, String contraseña, String tipo) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    
        /*
           if(val){
               System.out.println("Usuario y contraseña Correctos!");
           }else{
               System.out.println("Usuario y contraseña Incorrectos!");
         */
    
    //public void 

    @Override
    public String toString() {
        return "Usuario{" + "nombre=" + nombre + ", apellido=" + apellido + ", usuario=" + usuario + ", contrase\u00f1a=" + contraseña + ", tipo=" + tipo + '}';
    }

}


   
   
    

