/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuarios;

import Usuarios.Paciente;
import Usuarios.Paciente;
import Usuarios.Usuario;
import Usuarios.Usuario;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import plataformamedica.LeerFichero;

/**
 *
 * @author Natanael
 */
public class Asistente extends Usuario {

    public Asistente(String nombre, String apellido, String usuario, String contraseña, String tipo) {
        super(nombre, apellido, usuario, contraseña, tipo);
    }

    //Actualizar datos
    public void actualizarDatos(ArrayList<Usuario> usuarios, String nombreArchivo) {
        Scanner sc = new Scanner(System.in);
        boolean valor = false;
        System.out.println("*******Llenar Paciente*********");
        System.out.println("                               ");
        System.out.print("Ingrese el Usuario: ");
        String user = sc.nextLine();
        for (Usuario u : usuarios) {
            if ((u instanceof Paciente) && (((Paciente) u).getUsuario().equals(user))) {

                System.out.print("Ingrese Cedula: ");
                ((Paciente) u).setCedula(sc.nextLine());
                System.out.print("Ingrese Edad: ");
                ((Paciente) u).setEdad(sc.nextInt());
                System.out.print("Ingrese Peso ");
                ((Paciente) u).setPeso(sc.nextDouble());
                System.out.print("Ingrese Estatura: ");
                ((Paciente) u).setEstatura(sc.nextDouble());
                System.out.println("Ingrese fecha de Nacimiento (dia/mes/año)");
                ((Paciente) u).setFechaNacimiento(new Date(sc.nextInt(), sc.nextInt(), sc.nextInt()));
                sc.nextLine();
                System.out.print("Ingrese Genero (F/M)");
                ((Paciente) u).setGenero(sc.nextLine());
                LeerFichero.actualizarPaciente("perfilPaciente.txt", (Paciente) u);
                valor = true;
            }

        }
        if (valor == false) {
            System.out.println("Error!");

        }
    }

    public void menu(Asistente u, ArrayList<Usuario> usuarios) {
        Scanner sc = new Scanner(System.in);
        System.out.println("*******Menu Asistente********");
        System.out.println("1. Llenar perfil del paciente");
        System.out.println("2. Cancelar Cita");
        System.out.println("3. Cancelar usuario");
        System.out.println("4. Consultar pacientes con Cita");
        System.out.println("5. Salir");
        System.out.print("Elija un opcion:");
        int opc = sc.nextInt();
        switch (opc) {
            case 1:
                u.actualizarDatos(usuarios, "perfilPaciente.txt");
                menu(u, usuarios);
                break;
            case 2:
                u.cancelarCita("citas_medicas.txt", "solicitud_de_cancelacion.txt");
                menu(u, usuarios);
                break;
            case 3:
                u.cancelarUsuario();
                menu(u, usuarios);
                break;
            case 4:
                u.ConsultarCitaAsistente("citas_medicas.txt", usuarios);
                menu(u, usuarios);
                break;
            case 5:
                System.out.println("Gracias por usar nuestra plataforma!");
                break;
            default:
                System.out.println("Ingrese una opcion valida!");
                menu(u, usuarios);
                break;
        }

    }

    public void cancelarCita(String citaMedica, String solicitud) {
        Scanner sc = new Scanner(System.in);
        LeerFichero l1 = new LeerFichero();
        System.out.print("Ingrese el codigo a eliminar: ");
        String codigos = sc.nextLine();
        ArrayList<String> cod = l1.extraerCita("citas_medicas.txt",codigos);
        String nombre=l1.extraerNombre("citas_medicas.txt", codigos);
        l1.Borrarcita("citas_medicas.txt", codigos, cod);
        System.out.println("Estimado asistente, la cita del cliente "+nombre+", con código "+codigos+" ha sido cancelada.");
        l1.eliminarSolicitud("solicitud_de_cancelacion.txt", codigos, cod);

    }

    public void cancelarUsuario() {
        Scanner sc = new Scanner(System.in);
        LeerFichero l1 = new LeerFichero();
        System.out.print("Ingrese el usuario a eliminar: ");
        String user = sc.nextLine();
        ArrayList<String> usuarios = l1.SacarUsusarioPendientes("usuarios.txt", user);
        l1.Borrarusuario("usuarios.txt", user, usuarios);
        String nombre=l1.extraerNombre1("citas_medicas.txt", user);
        System.out.println("Estimado asistente, se procederá a desactivar el usuario del cliente "+nombre+", en las próximas 48 horas, desde este momento ya no puede hacer transacciones de reserva en el sistema.");
    }

    public void ConsultarCitaAsistente(String nombrearchivo, ArrayList<Usuario> usuarios) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {

            archivo = new File(nombrearchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            br.readLine();
            int i = 0;

            while ((linea = br.readLine()) != null && i == 0) {

                String[] datos = linea.split(",");

                for (Usuario u : usuarios) {

                    if (datos[3].equals(u.getNombre() + " " + u.getApellido())) {
                        ArrayList<String> lineas = new ArrayList<String>();
                        File archivo1 = null;
                        FileReader fr1 = null;
                        BufferedReader br1 = null;
                       // boolean valor = false;
                        try {

                            archivo1 = new File("perfilPaciente.txt");
                            fr1 = new FileReader(archivo1);
                            br1 = new BufferedReader(fr1);

                            // Lectura del fichero
                            String linea1;
                            br1.readLine();
                            int i1 = 0;

                            while ((linea1 = br1.readLine()) != null && i1 == 0) {
                                lineas.add(linea1);

                            }

                            for (String y : lineas) {
                                String[] datos1 = y.split(",");
                                if(datos1[0].equals(u.getUsuario().toString())) {
                                    ((Paciente)u).setEdad(Integer.parseInt(datos1[2]));
                                }

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {

                            try {
                                if (null != fr) {
                                    fr.close();
                                }
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                        }

                        System.out.println("-------CITAS AGENDADAS-------");
                        System.out.println(datos[3] + ", " + ((Paciente) u).getEdad() + " años");
                    }

                }
                i++;

            }
            if (i == 0) {
                System.out.println("No existen citas registradas");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void modificarArchivoCitas(ArrayList<String> auxiliar, String citaMedica) {

        FileWriter fichero = null;
        BufferedWriter bw = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter(citaMedica, true);
            bw = new BufferedWriter(fichero);

            for (String s : auxiliar) {
                bw.write(s + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (null != fichero) {
                    //fichero.close();
                    bw.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }




}

