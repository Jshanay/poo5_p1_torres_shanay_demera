/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuarios;

import Usuarios.Usuario;
import Usuarios.Usuario;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import plataformamedica.LeerFichero;

/**
 *
 * @author Gustavo Torres
 */
public class Paciente extends Usuario {

    protected String usuario;
    protected String cedula;
    protected int edad;
    protected double peso;
    protected double estatura;
    protected Date fechaNacimiento = new Date(0, 0, 0);
    protected String genero;

    Scanner sc = new Scanner(System.in);

    public Paciente(String nombre, String apellido, String usuario, String contraseña, String tipo) {
        super(nombre, apellido, usuario, contraseña, tipo);
    }

    public double getEstatura() {
        return estatura;
    }

    public void setEstatura(double estatura) {
        this.estatura = estatura;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return super.usuario + "," + cedula + "," + edad + "," + peso + "," + estatura + "," + fechaNacimiento + "," + genero;
    }

    public void SolicitarCita() {

        String confirmacion = "N";

        while (confirmacion.equalsIgnoreCase("N")) {

            String[] listaEspecialidades = {"General", "Cardiologia", "Oncologia"};
            System.out.println("-----------------ESPECIALIDADES-----------------");
            System.out.println("---------Presione F para volver al menu---------");
            for (int a = 0; a < listaEspecialidades.length; a++) {
                System.out.println((a + 1) + ")" + listaEspecialidades[a]);

            }

            System.out.print("Escoja especialidad:");

            String opcion = sc.nextLine();
            if (opcion.equalsIgnoreCase("f")) {
                confirmacion = "S";
            } else {

                int especialidad = Integer.parseInt(opcion);
                String especialidadEscojida = listaEspecialidades[especialidad - 1];

                LeerFichero ficheroDoctor = new LeerFichero();
                ArrayList<String> doctores = new ArrayList<String>();

                ficheroDoctor.leerDoctores("Doctores.txt", doctores);

                String opcionDoctor = sc.nextLine();

                if (opcionDoctor.equalsIgnoreCase("f")) {
                    confirmacion = "S";
                } else {
                    int doctor = Integer.parseInt(opcionDoctor);

                    String doctorEscojido = doctores.get(doctor - 1);

                    LeerFichero ficheroHorario = new LeerFichero();
                    ArrayList<String> horarios = new ArrayList<String>();

                    ficheroHorario.leerHorarios("HorariosDisponibles.txt", horarios);

                    String opcionHorario = sc.nextLine();

                    if (opcionHorario.equalsIgnoreCase("f")) {
                        confirmacion = "S";
                    } else {
                        int horario = Integer.parseInt(opcionHorario);

                        String horarioEscojido = horarios.get(horario - 1);

                        System.out.print("Desea seleccionar la cita con los datos seleccionados (S/N): ");

                        String respuesta = sc.nextLine();

                        if (respuesta.equalsIgnoreCase("s")) {
                            confirmacion = "S";
                            int aleatorio = 0;
                            aleatorio = (int) (Math.random() * 99999);
                            String codigo = Integer.toString(aleatorio);

                            LeerFichero ficheroCita = new LeerFichero();
                            ficheroCita.CrearCita("citas_medicas.txt",
                                    codigo, especialidadEscojida, doctorEscojido, this.getNombre() + " " + this.getApellido(), horarioEscojido);
                            System.out.println("Su cita se ha reservado con el codigo: " + codigo);

                        }

                    }

                }
            }

        }

    }

    public void ConsultarCita() {

        LeerFichero ficheroConsultar = new LeerFichero();

        System.out.println("-----------------CONSULTAR  CITA PENDIENTE-----------------");
        System.out.print("Ingrese el codigo de su cita: ");
        String codigo = sc.nextLine();
        ficheroConsultar.ConsultarCita("citas_medicas.txt", codigo);

    }

    public void SolicitarCancelacion() {
        System.out.println("-----------------SOLICITUD DE CANCELACION DE CITA-----------------");

        LeerFichero ficheroCancelar = new LeerFichero();
        System.out.print("Ingrese el codigo:");

        String codigo = sc.nextLine();

        ficheroCancelar.CancelarCita("citas_medicas.txt", "solicitud_de_cancelacion.txt", codigo, this.getNombre() + " " + this.getApellido());

    }

    public void menu(Paciente u) {
        Scanner sc = new Scanner(System.in);
        LeerFichero f1 = new LeerFichero();
        System.out.println("*******Menu Paciente********");
        System.out.println("1. Solicitar una Cita Medica");
        System.out.println("2. Consultar cita pendiente");
        System.out.println("3. Solicitar cancelacion de Cita");
        System.out.println("4. Salir");
        System.out.print("Elija un opcion:");
        int opc = sc.nextInt();
        switch (opc) {
            case 1:
                if (f1.LeerPerfilPaciente("perfilPaciente.txt", u.getUsuario().toString())) {
                    u.SolicitarCita();
                } else {
                    System.out.println("Error!");
                    System.out.println("No existen datos actualizados del paciente.");
                }

                menu(u);
                break;
            case 2:
                u.ConsultarCita();
                menu(u);
                break;
            case 3:
                u.SolicitarCancelacion();
                menu(u);
                break;
            case 4:
                System.out.println("Gracias!");
                break;
            default:
                System.out.println("Ingrese una opcion valida!");
                menu(u);
                break;
        }
    }
}
